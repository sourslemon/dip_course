
#include <stdint.h>

#pragma pack(1)

#define BMP_HEADER_SIZE 54
#define BMP_Palette_SIZE 1024

typedef struct __bmp_file_header {  // 14 bytes

    uint8_t  Type[2];
    uint32_t File_Size;
    uint32_t Reserved;
    uint32_t Data_Offset;
} FileHeader_t;

typedef struct __bmp_info_header {  // 40 bytes

    uint32_t Header_Size;
    uint32_t Width;
    uint32_t Height;
    uint16_t Planes;
    uint16_t Bits_Per_Pixel;
    uint32_t Compression;
    uint32_t Data_Size;
    uint32_t H_Resolution;
    uint32_t V_Resolution;
    uint32_t Used_Colors;
    uint32_t Important_Colors;

} InfoHeader_t;

typedef struct __bmp_file {
    FileHeader_t FileHeader;
    InfoHeader_t InfoHeader;
    uint8_t Palette[BMP_Palette_SIZE];
    uint32_t Data_lenth;
    uint8_t Padding;
    uint8_t *Data;
}Bmp_t;


