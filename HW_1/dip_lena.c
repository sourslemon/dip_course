/**
 * @file dip_lena.c
 * @author Yi-Mou (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2019-09-27
 *
 * @copyright Copyright (c) 2019
 *
 */
#include "dip_lena.h"

#include <stdio.h>
#include <stdlib.h>

void change8_bit(Bmp_t* bitmap);
void change_position(Bmp_t* bitmap);
void show_header(Bmp_t* bitmap);
void plot(uint16_t* histogram);
int main(int argc,char **argv) {
    Bmp_t bmp;

    //char a[20]="airplane.bmp";

    FILE* f = fopen(argv[1], "rb");
    if(f == NULL){
        printf("error\n");
        return 1;
    }

    fread(&bmp, sizeof(uint8_t), BMP_HEADER_SIZE, f);

    show_header(&bmp);

    if(bmp.InfoHeader.Width%4 != 0){
        bmp.Padding=4-(bmp.InfoHeader.Width%4);
    }
    

    bmp.Data_lenth = bmp.FileHeader.File_Size - bmp.FileHeader.Data_Offset;
    printf("%ld\n", bmp.Data_lenth);

    bmp.Data = (uint8_t*)calloc(bmp.Data_lenth, sizeof(uint8_t));
    if (bmp.Data == NULL) {
        printf("error\n");
        return 1;
    }

    fread(bmp.Data, sizeof(uint8_t), bmp.Data_lenth, f);

    fclose(f);

    FILE* f2 = fopen("result/origin.bmp", "wb");
    fwrite(&bmp, sizeof(uint8_t), bmp.FileHeader.Data_Offset, f2);
    fwrite(bmp.Data, sizeof(uint8_t), bmp.Data_lenth, f2);
    fclose(f2);

    change8_bit(&bmp);

    show_header(&bmp);

    FILE* f3 = fopen("result/gray.bmp", "wb");
    fwrite(&bmp, sizeof(uint8_t), BMP_HEADER_SIZE, f3);
    fwrite(bmp.Palette, sizeof(uint8_t), BMP_Palette_SIZE, f3);
    fwrite(bmp.Data, sizeof(uint8_t), bmp.Data_lenth, f3);
    fclose(f3);

    change_position(&bmp);

    FILE* f4 = fopen("result/slice.bmp", "wb");
    fwrite(&bmp, sizeof(uint8_t), BMP_HEADER_SIZE, f4);
    fwrite(bmp.Palette, sizeof(uint8_t), BMP_Palette_SIZE, f4);
    fwrite(bmp.Data, sizeof(uint8_t), bmp.Data_lenth, f4);
    fclose(f4);

    FILE* f5 = fopen("result/hitogram.csv", "wb");

    uint16_t hisrogram[256] = {0};
    for (uint32_t i = 0; i < bmp.Data_lenth; i++) {
        hisrogram[*(bmp.Data + i)]++;
    }
    for (int i = 0; i < 256; i++) {
        fprintf(f5, "%d\n", hisrogram[i]);
    }
    fclose(f5);

    plot(hisrogram);

    free(bmp.Data);

    return 0;
}

void change8_bit(Bmp_t* bitmap) {
    bitmap->InfoHeader.Data_Size = 0;
    bitmap->FileHeader.Data_Offset = BMP_HEADER_SIZE + BMP_Palette_SIZE;
    
    bitmap->FileHeader.File_Size =
        bitmap->FileHeader.Data_Offset +
        (bitmap->InfoHeader.Height * (bitmap->InfoHeader.Width+bitmap->Padding)); //+ bitmap->Padding
    bitmap->InfoHeader.Bits_Per_Pixel = 8;

    for (uint32_t i = 0; i < 256; i++) {   // 0xxRRGGBB
        bitmap->Palette[(4 * i)] = i;      // B
        bitmap->Palette[(4 * i) + 1] = i;  // G
        bitmap->Palette[(4 * i) + 2] = i;  // R
        bitmap->Palette[(4 * i) + 3] = 0;
    }
    bitmap->Data_lenth =
        bitmap->FileHeader.File_Size - bitmap->FileHeader.Data_Offset;
    
    printf("Padding: %d\n",bitmap->Padding);
    for (uint32_t i = 0; i < bitmap->InfoHeader.Height; i++){
        for (uint32_t j = 0; j < bitmap->InfoHeader.Width; j++){
            bitmap->Data[((bitmap->InfoHeader.Width+bitmap->Padding)*i)+j] =
            (uint8_t)((0.114 * bitmap->Data[((3*bitmap->InfoHeader.Width+bitmap->Padding)*i)+3*j]) +      // B
                      (0.587 * bitmap->Data[((3*bitmap->InfoHeader.Width+bitmap->Padding)*i)+3*j+1]) +  // G
                      (0.299 * bitmap->Data[((3*bitmap->InfoHeader.Width+bitmap->Padding)*i)+3*j+2]));  // R
        }
    }
    bitmap->Data =
        (uint8_t*)realloc(bitmap->Data, bitmap->Data_lenth * sizeof(uint8_t));
    if (bitmap->Data == NULL) {
        printf("error\n");
        return;
    }
}

void change_position(Bmp_t* bitmap) {
    uint8_t temp = 0;
    /*
    for (int i = 0; i < 256; i++) {
        for (int j = 0; j < 256; j++) {
            temp                          = bitmap->Data[(512*i)+j];
            bitmap->Data[(512*i)+j]       = bitmap->Data[(512*i)+j+256];
            bitmap->Data[(512*i)+j+256]   = bitmap->Data[(512*(i+256))+j];
            bitmap->Data[(512*(i+256))+j] = temp;
        }
    }
    */
    for (int i = 0; i < (bitmap->InfoHeader.Height / 2); i++) {
        for (int j = 0; j < (bitmap->InfoHeader.Width / 2); j++) {
            temp = bitmap->Data[(bitmap->InfoHeader.Width * i) + j];

            bitmap->Data[(bitmap->InfoHeader.Width * i) + j] =
                bitmap->Data[(bitmap->InfoHeader.Width * i) + j +
                             (bitmap->InfoHeader.Width / 2)];

            bitmap->Data[(bitmap->InfoHeader.Width * i) + j +
                         (bitmap->InfoHeader.Width / 2)] =
                bitmap->Data[(bitmap->InfoHeader.Width *
                              (i + (bitmap->InfoHeader.Height / 2))) +
                             j];

            bitmap->Data[(bitmap->InfoHeader.Width *
                          (i + (bitmap->InfoHeader.Height / 2))) +
                         j] = temp;
        }
    }
}

void show_header(Bmp_t* bitmap) {
    printf("Type: '%c''%c'\n", bitmap->FileHeader.Type[0],
           bitmap->FileHeader.Type[1]);
    printf("File_Size: %ld bytes\n", bitmap->FileHeader.File_Size);
    printf("Reserved: %ld\n", bitmap->FileHeader.Reserved);
    printf("Data_Offset: %ld bytes\n", bitmap->FileHeader.Data_Offset);
    printf("Header_Size: %ld bytes\n", bitmap->InfoHeader.Header_Size);
    printf("Width: %ld\n", bitmap->InfoHeader.Width);
    printf("Height: %ld\n", bitmap->InfoHeader.Height);
    printf("Planes: %d\n", bitmap->InfoHeader.Planes);
    printf("Bits_Per_Pixel: %d\n", bitmap->InfoHeader.Bits_Per_Pixel);
    printf("Compression: %ld\n", bitmap->InfoHeader.Compression);
    printf("Data_Size: %ld bytes\n", bitmap->InfoHeader.Data_Size);
    printf("H_Resolution: %ld\n", bitmap->InfoHeader.H_Resolution);
    printf("V_Resolution: %ld\n", bitmap->InfoHeader.V_Resolution);
    printf("Used_Colors:% ld\n", bitmap->InfoHeader.Used_Colors);
    printf("Important_Colors: %ld\n", bitmap->InfoHeader.Important_Colors);
}

void plot(uint16_t* histogram){
    uint16_t max_gray=0;
    for(uint32_t i=0;i<256;i++){
        if (histogram[i] > max_gray){
            max_gray=histogram[i];
        }
    }
    for(long int i=max_gray;i>=0;i=i-50){
        for(uint16_t j=0;j<256;j++){
            if(histogram[j]>= i){
                printf("|");
            }
            else{
                printf(" ");
            }
        }
        printf("\n");
    }
}
